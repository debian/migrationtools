#!/bin/sh
#
# $Id: migrate_all_nis_online.sh,v 1.4 2004/09/24 05:49:08 lukeh Exp $
#
# Copyright (c) 1997-2003 Luke Howard.
# Copyright (c) 2020 Tanya.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#        This product includes software developed by Luke Howard.
# 4. The name of the other may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE LUKE HOWARD ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL LUKE HOWARD BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#

#
# Migrate NIS/YP accounts using ldapadd
#

# Defaults
PATH=$PATH:.
export PATH
dirname=$(dirname "$0")

# Import
if [ -f "$dirname/migrate_common.sh" ]; then
	. "$dirname/migrate_common.sh"
elif [ -f "/usr/share/migrationtools/migrate_common.sh" ]; then
	. "/usr/share/migrationtools/migrate_common.sh"
fi

# Set perl vars
perlset
# Get LDAP data
readldapconf "/etc/ldap/ldap.conf"
# Get arguments
on_getopts $@
# Generate temp folder
tmpfd_gen

question="Enter the NIS domain to import from (optional): "
echo "$question " | tr -d '\012' > /dev/tty
read DOM
if [ "X$DOM" = "X" ]; then
	DOMFLAG=""
else
	DOMFLAG="-d $DOM"
fi

ypcat $DOMFLAG passwd > $ETC_PASSWD
ypcat $DOMFLAG group > $ETC_GROUP
if [ "$MIGRATION_TYPE" != 'min' ]; then
	ypcat $DOMFLAG services > $ETC_SERVICES
	ypcat $DOMFLAG protocols > $ETC_PROTOCOLS
	ypcat $DOMFLAG rpc.byname > $ETC_RPC
	ypcat $DOMFLAG hosts > $ETC_HOSTS
	ypcat $DOMFLAG networks > $ETC_NETWORKS
	#ypcat $DOMFLAG -k aliases > $ETC_ALIASES
fi

# Call main functions
on_main
# Destroy temp folder
tmpfd_destroy
