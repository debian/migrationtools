# Migrationtools

Migration scripts for LDAP

Table of Contents
=================

   * [Migrationtools](#migrationtools)
      * [Introduction](#introduction)
         * [Set-up](#set-up)
         * [Usage](#usage)
            * [Import tools table](#import-tools-table)
            * [Environment variables](#environment-variables)
         * [Copyright and license](#copyright-and-license)

## Introduction

These tools are freely redistributable according to the license
included with the source files. They may be bundled with LDAP/NIS
migration products. See RFC 2307 for more information.

You need perl to run these.

## Set-up

Modifying migrate_common.ph is not required anymore, this script is able to autoconfigure using /etc/ldap/ldap.conf client settings.

URI and BASE are necessary for autoconf, otherwise you'll have to input data by yourself

## Usage

You can use them one by one like this example:

    ./migrate_protocols.pl /etc/protocols protocols.ldif

or migrate everything using one of the scripts from the table below, online are meant to be used when the ldap service is active, offline vice versa

N.B. while using perl scripts, if you won't provide an output file they'll output to stdout

### Import tools table

    Script                          Existing nameservice    LDAP online
    ===================================================================
    migrate_all_online.sh           /etc flat files         YES
    migrate_all_offline.sh          /etc flat files         NO
    migrate_all_netinfo_online.sh   NetInfo                 YES
    migrate_all_netinfo_offline.sh  NetInfo                 NO
    migrate_all_nis_online.sh       Sun NIS/YP              YES
    migrate_all_nis_offline.sh      Sun NIS/YP              NO
    migrate_all_nisplus_online.sh   Sun NIS+                YES
    migrate_all_nisplus_offline.sh  Sun NIS+                NO

    (The online scripts use ldapadd; the offline scripts use slapadd or ldif2ldbm.)

### Environment variables

Additional customizations are available by using environment variables

 - General:
   - LDAP_EXTENDED_SCHEMA	Enables Kerberos and SAMBA attribute exports, useful for old-type NT4 domain
   - SAMBA_DOMAIN_SID		Enables SAMBA attribute exports if it's a valid SID, disables them if it's unvalid, provides/changes samba domain SID
   - UID/GID_BELOW/ABOVE	Excludes UID/GID not matching the settings
   - DEFAULT_MAIL_HOST		Self-descriptive
   - LDAP_BASEDN			If BASE isn't provided by /etc/ldap/ldap.conf it can be specified here
   - LDAP
 - Shell only:
   - MIGRATION_TYPE			'full' for full migration, 'min' for only users and groups exports, default is full migration. Another way of changing this setting is '-m' option
   - SASL_MECH				Changes AUTH/SASL mechanism, -a takes precedence
   - LDAPHOST				If URI isn't provided by /etc/ldap/ldap.conf it can be specified here
   - LDAP_BINDDN			Bind as this user
   - LDAP_BINDCRED			You can insert your password here but it's not advised to do so, let the scripts ask it
   - LDAP_PROFILE			Push DUAProfile, set it to no if you don't know and don't want the scripts to ask you

Note: script options **always** take precedence over env variables

## Copyright and license

License is at the start of each file

THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED
WARRANTY AND WITHOUT SUPPORT.

Copyright (C) 1996-2001 Luke Howard. All rights reserved.

Copyright (C) 2020 Tanya. All rights reserved. 
